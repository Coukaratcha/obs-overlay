import requests
import json

class Twitch():
	def __init__(self, login, client_id, client_secret):
		self.request_token_url = "https://id.twitch.tv/oauth2/token"
		self.base_url = "https://api.twitch.tv/helix"
		self.client_id = client_id
		self.client_secret = client_secret
		self.login = login
		self.access_token = None
		self.headers = {}

	def get_token(self):
		payload = {
			"client_id": self.client_id,
			"client_secret": self.client_secret,
			"grant_type": "client_credentials",
			"scope": " ".join(("user:read:follows", "user:read:subscriptions"))
		}
		r = requests.post(self.request_token_url, params=payload)

		if r.status_code == 200:
			data = r.json()

			self.access_token = data["access_token"]
			self.headers = {
				"Authorization": "Bearer {access_token}".format(access_token=self.access_token),
				"Client-Id": self.client_id
			}
		else:
			raise Exception("Cannot get token \nstatus_code: {} \ncontent: {}".format(r.status_code, r.json()))

	def get_user(self):
		payload = {
			"login": self.login
		}
		r = requests.get("{base_url}/users".format(base_url=self.base_url), params=payload, headers=self.headers)
		
		return r.json()["data"][0]

	def get_followers(self, uid):
		payload = {
			"to_id": uid
		}
		r = requests.get("{base_url}/users/follows".format(base_url=self.base_url), params=payload, headers=self.headers)

		return r.json()

	def get_stream(self):
		payload = {
			"user_login": self.login
		}
		r = requests.get("{base_url}/streams".format(base_url=self.base_url), params=payload, headers=self.headers)

		return r.json()

	def get_info(self):
		user = self.get_user()
		uid = user["id"]

		followers = self.get_followers(uid)
		stream = self.get_stream()

		res = {
			"last_follower": followers["data"][0]["from_name"],
			"followers_count": followers["total"],
			"viewers_count": stream["data"][0]["viewer_count"] if stream["data"] else 0
		}
		return res 
