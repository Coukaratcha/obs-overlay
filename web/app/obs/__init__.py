from flask import (
	Flask,
	session,
	jsonify,
	redirect,
	url_for,
	render_template
)
from .config import Config
from .twitch import Twitch

app = Flask(__name__)
app.config.from_object(Config)

twitch = Twitch(
	login = app.config["TWITCH_LOGIN_NAME"],
	client_id = app.config["TWITCH_OAUTH_CLIENT_ID"],
	client_secret = app.config["TWITCH_OAUTH_CLIENT_SECRET"]
)

@app.route("/")
@app.route("/labels")
def home():
	twitch.get_token()
	info = twitch.get_info()
	return render_template("labels.html", info=info)

@app.route("/bar-goal")
def bar_goal():
	twitch.get_token()
	info = twitch.get_info()
	return render_template("bar-goal.html", info=info)

@app.route("/info")
def info():
	twitch.get_token()
	return jsonify(twitch.get_info())