import os

class Config(object):
	TWITCH_OAUTH_CLIENT_ID = os.environ.get("TWITCH_OAUTH_CLIENT_ID")
	TWITCH_OAUTH_CLIENT_SECRET = os.environ.get("TWITCH_OAUTH_CLIENT_SECRET")
	TWITCH_LOGIN_NAME = os.environ.get("TWITCH_LOGIN_NAME")
	SECRET_KEY = os.environ.get("SECRET_KEY")